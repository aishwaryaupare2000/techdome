FROM node:latest
WORKDIR /app
COPY . .
RUN npm install
COPY ./app .
EXPOSE 3000
CMD ["node", "app.js"]
